# README

## Running
In order to run the project, you need to run the folowing steps:
1. Build images, run `make build`
2. Install dependencies and setup database, run `make prepare`
3. Run the application, run `make start`

The application will be accessible trough localhos:8080

## Testing
Follow at least the previous 1 and 2 steps, and run `make test`