<?php

namespace App\Tests\Functional;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class User extends WebTestCase
{
    public function testCreateUser()
    {
        $client = static::createClient();

        $params = ['name' => uniqid('user_')];
        $client->request('POST', '/users', $params);

        $this->assertEquals(201, $client->getResponse()->getStatusCode());

        $response = json_decode($client->getResponse()->getContent());

        $this->assertEquals($response->name, $params['name']);
        $this->assertEquals(strlen($response->access_key), 32);
    }

    public function testCreateUserWithoutName()
    {
        $client = static::createClient();

        $client->request('POST', '/users');

        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }

    public function testListUsers()
    {
        $client = static::createClient();

        $client->request('GET', '/users');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $response = json_decode($client->getResponse()->getContent());

        $this->assertGreaterThanOrEqual(1, count($response));
    }

    public function testUpdateUser()
    {
        $client = static::createClient();

        $initalParams = ['name' => uniqid('user_')];
        $client->request('POST', '/users', $initalParams);

        $this->assertEquals(201, $client->getResponse()->getStatusCode());

        $createResponse = json_decode($client->getResponse()->getContent());

        $newParams = [
            'name' => uniqid('user_'),
            'access_key' => $createResponse->access_key
        ];

        $client->request('PATCH', '/users', $newParams);
        $updateResponse = json_decode($client->getResponse()->getContent());

        $this->assertEquals($newParams['name'], $updateResponse->name);
        $this->assertEquals($createResponse->id, $updateResponse->id);
    }

    public function testUpdateUserWithInvalidKey()
    {
        $client = static::createClient();

        $params = [
            'name' => uniqid('user_'),
            'access_key' => 'non_existent_key'
        ];

        $client->request('PATCH', '/users', $params);

        $response = json_decode($client->getResponse()->getContent());

        $this->assertEquals(401, $client->getResponse()->getStatusCode());
    }
}
