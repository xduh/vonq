<?php

namespace App\Tests\Functional;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class Meeting extends WebTestCase
{

    protected $userAccessKey;
    protected $userName;
    protected $organizationId;

    protected function setUp()
    {
        $client = static::createClient();

        $this->userName = uniqid('user_');
        $params = ['name' => $this->userName];
        $client->request('POST', '/users', $params);

        $response = json_decode($client->getResponse()->getContent());

        $this->userAccessKey = $response->access_key;

        $client = static::createClient();

        $params = [
            'name' => uniqid('my_organization_name'),
            'access_key' => $this->userAccessKey
        ];

        $client->request('POST', '/organizations', $params);
        $response = json_decode($client->getResponse()->getContent());

        $this->organizationId = $response->id;
    }

    public function testCreateMeeting()
    {
        $client = static::createClient();

        $meetingDate = new \DateTime();
        $meetingDate->modify('+2 day');

        $params = [
            'title' => uniqid('my_organization_name'),
            'date' => $meetingDate->format('Y-m-d H:i:s'),
            'access_key' => $this->userAccessKey
        ];


        $route = sprintf('/organizations/%d/meetings', $this->organizationId);

        $client->request('POST', $route, $params);
        $this->assertEquals(201, $client->getResponse()->getStatusCode());
    }
}
