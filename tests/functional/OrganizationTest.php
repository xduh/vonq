<?php

namespace App\Tests\Functional;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class Organization extends WebTestCase
{

    protected $userAccessKey;
    protected $userName;

    protected function setUp()
    {
        $client = static::createClient();

        $this->userName = uniqid('user_');
        $params = ['name' => $this->userName];
        $client->request('POST', '/users', $params);

        $response = json_decode($client->getResponse()->getContent());

        $this->userAccessKey = $response->access_key;
    }

    public function testCreateOrganization()
    {
        $client = static::createClient();

        $params = [
            'name' => uniqid('my_organization_name'),
            'access_key' => $this->userAccessKey
        ];
        $client->request('POST', '/organizations', $params);

        $this->assertEquals(201, $client->getResponse()->getStatusCode());

        $response = json_decode($client->getResponse()->getContent());

        $this->assertEquals($response->name, $params['name']);
    }

    public function testCreateOrganizationWithoutName()
    {
        $client = static::createClient();

        $params = [
            'access_key' => $this->userAccessKey
        ];
        $client->request('POST', '/organizations', $params);

        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }

    public function testListOrganizations()
    {
        $client = static::createClient();

        $params = [
            'name' => uniqid('my_organization_name'),
            'access_key' => $this->userAccessKey
        ];
        $client->request('POST', '/organizations', $params);

        $this->assertEquals(201, $client->getResponse()->getStatusCode());

        $client->request('GET', '/organizations');
        $response = json_decode($client->getResponse()->getContent());
        $this->assertGreaterThanOrEqual(1, count($response));
    }

    public function testSubscribeOrganization()
    {
        $client = static::createClient();

        $params = [
            'name' => uniqid('my_organization_name'),
            'access_key' => $this->userAccessKey
        ];
        $client->request('POST', '/organizations', $params);

        $this->assertEquals(201, $client->getResponse()->getStatusCode());

        $response = json_decode($client->getResponse()->getContent());

        $params = [
            'access_key' => $this->userAccessKey
        ];

        $route = sprintf('/organizations/%d/subscribe', $response->id);
        $client->request('POST', $route, $params);
        $this->assertEquals(201, $client->getResponse()->getStatusCode());
    }

    public function testSubscribeOrganizationTwice()
    {
        $client = static::createClient();

        $params = [
            'name' => uniqid('my_organization_name'),
            'access_key' => $this->userAccessKey
        ];
        $client->request('POST', '/organizations', $params);

        $this->assertEquals(201, $client->getResponse()->getStatusCode());

        $response = json_decode($client->getResponse()->getContent());

        $params = [
            'access_key' => $this->userAccessKey
        ];

        $route = sprintf('/organizations/%d/subscribe', $response->id);
        $client->request('POST', $route, $params);
        $this->assertEquals(201, $client->getResponse()->getStatusCode());

        $client->request('POST', $route, $params);
        $this->assertEquals(403, $client->getResponse()->getStatusCode());
    }

    public function testLeaveOrganization()
    {
        $client = static::createClient();

        $params = [
            'name' => uniqid('my_organization_name'),
            'access_key' => $this->userAccessKey
        ];
        $client->request('POST', '/organizations', $params);

        $this->assertEquals(201, $client->getResponse()->getStatusCode());

        $response = json_decode($client->getResponse()->getContent());

        $params = [
            'access_key' => $this->userAccessKey
        ];

        $route = sprintf('/organizations/%d/subscribe', $response->id);
        $client->request('POST', $route, $params);
        $this->assertEquals(201, $client->getResponse()->getStatusCode());

        $route = sprintf('/organizations/%d/leave', $response->id);
        $client->request('POST', $route, $params);

        $this->assertEquals(204, $client->getResponse()->getStatusCode());
    }
}
