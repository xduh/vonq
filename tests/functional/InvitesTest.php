<?php

namespace App\Tests\Functional;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class Invite extends WebTestCase
{
    protected $userAccessKey;
    protected $userName;

    protected function setUp()
    {
        $client = static::createClient();

        $this->userName = uniqid('user_');
        $params = ['name' => $this->userName];
        $client->request('POST', '/users', $params);

        $response = json_decode($client->getResponse()->getContent());

        $this->userAccessKey = $response->access_key;
    }

    public function testInvite()
    {
        $client = static::createClient();

        $recipientName = uniqid('user_');

        $params = ['name' => $recipientName];
        $client->request('POST', '/users', $params);

        $params = [
            'access_key' => $this->userAccessKey,
            'name' => $recipientName
        ];

        $client->request('POST', '/invites', $params);

        $this->assertEquals(201, $client->getResponse()->getStatusCode());
    }

    public function testRepeatedInvite()
    {
        $client = static::createClient();

        $recipientName = uniqid('user_');

        $params = ['name' => $recipientName];
        $client->request('POST', '/users', $params);

        $params = [
            'access_key' => $this->userAccessKey,
            'name' => $recipientName
        ];

        $client->request('POST', '/invites', $params);
        $client->request('POST', '/invites', $params);

        $this->assertEquals(403, $client->getResponse()->getStatusCode());
    }

    public function testInviteYourself()
    {
        $client = static::createClient();

        $params = [
            'access_key' => $this->userAccessKey,
            'name' => $this->userName
        ];

        $client->request('POST', '/invites', $params);

        $this->assertEquals(403, $client->getResponse()->getStatusCode());
    }

    public function testCrossedInvite()
    {
        $client = static::createClient();

        $params = ['name' => uniqid('user_')];
        $client->request('POST', '/users', $params);
        $anotherUser = json_decode($client->getResponse()->getContent());

        $params = [
            'access_key' => $this->userAccessKey,
            'name' => $anotherUser->name
        ];

        $client->request('POST', '/invites', $params);
        $this->assertEquals(201, $client->getResponse()->getStatusCode());

        $params = [
            'access_key' => $anotherUser->access_key,
            'name' => $this->userName
        ];
        $this->assertEquals(201, $client->getResponse()->getStatusCode());

        $params = [
            'access_key' => $anotherUser->access_key,
        ];
        $client->request('GET', '/invites', $params);
        $response = json_decode($client->getResponse()->getContent());

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertCount(1, $response);
        $this->assertEquals($this->userName, $response[0]);
    }

    public function testConfirmInvite()
    {
        $client = static::createClient();

        $params = ['name' => uniqid('user_')];
        $client->request('POST', '/users', $params);
        $recipient = json_decode($client->getResponse()->getContent());

        $params = [
            'access_key' => $this->userAccessKey,
            'name' => $recipient->name
        ];

        $client->request('POST', '/invites', $params);

        $this->assertEquals(201, $client->getResponse()->getStatusCode());

        $params = [
            'access_key' => $recipient->access_key
        ];

        $route = sprintf('/invites/%s/confirm', $this->userName);
        $client->request('POST', $route, $params);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testInviteAlreadyConnectedUser()
    {
        $client = static::createClient();

        $params = ['name' => uniqid('user_')];
        $client->request('POST', '/users', $params);
        $recipient = json_decode($client->getResponse()->getContent());

        $params = [
            'access_key' => $this->userAccessKey,
            'name' => $recipient->name
        ];

        $client->request('POST', '/invites', $params);

        $this->assertEquals(201, $client->getResponse()->getStatusCode());

        $params = [
            'access_key' => $recipient->access_key
        ];

        $route = sprintf('/invites/%s/confirm', $this->userName);
        $client->request('POST', $route, $params);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $params = [
            'access_key' => $this->userAccessKey,
            'name' => $recipient->name
        ];

        $client->request('POST', '/invites', $params);
        $this->assertEquals(403, $client->getResponse()->getStatusCode());
    }
}
