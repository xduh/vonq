<?php

namespace App\Tests\Functional;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class Connection extends WebTestCase
{
    protected $userAccessKey;
    protected $userName;

    protected function setUp()
    {
        $client = static::createClient();

        $this->userName = uniqid('user_');
        $params = ['name' => $this->userName];
        $client->request('POST', '/users', $params);

        $response = json_decode($client->getResponse()->getContent());

        $this->userAccessKey = $response->access_key;
    }

    public function testListConnections()
    {
        $client = static::createClient();

        $params = ['name' => uniqid('user_')];
        $client->request('POST', '/users', $params);
        $recipient = json_decode($client->getResponse()->getContent());

        $params = [
            'access_key' => $this->userAccessKey,
            'name' => $recipient->name
        ];

        $client->request('POST', '/invites', $params);

        $this->assertEquals(201, $client->getResponse()->getStatusCode());

        $params = [
            'access_key' => $recipient->access_key
        ];

        $route = sprintf('/invites/%s/confirm', $this->userName);
        $client->request('POST', $route, $params);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $params = [
            'access_key' => $recipient->access_key
        ];

        $client->request('GET', '/connections', $params);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $response = $client->getResponse()->getContent();
        $this->assertEquals([$this->userName], json_decode($response));

        $params = [
            'access_key' => $this->userAccessKey
        ];

        $client->request('GET', '/connections', $params);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $response = $client->getResponse()->getContent();
        $this->assertEquals([$recipient->name], json_decode($response));
    }

    public function testListUserConnections()
    {
        $client = static::createClient();

        $params = ['name' => uniqid('user_')];
        $client->request('POST', '/users', $params);
        $recipient = json_decode($client->getResponse()->getContent());

        $params = [
            'access_key' => $this->userAccessKey,
            'name' => $recipient->name
        ];

        $client->request('POST', '/invites', $params);

        $this->assertEquals(201, $client->getResponse()->getStatusCode());

        $params = [
            'access_key' => $recipient->access_key
        ];

        $route = sprintf('/invites/%s/confirm', $this->userName);
        $client->request('POST', $route, $params);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $params = [
            'access_key' => $recipient->access_key
        ];
        $route = sprintf('/users/%s/connections', $recipient->name);
        $client->request('GET', $route, $params);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $response = $client->getResponse()->getContent();
        $this->assertEquals([$this->userName], json_decode($response));

        $params = [
            'access_key' => $this->userAccessKey
        ];
        $route = sprintf('/users/%s/connections', $this->userName);
        $client->request('GET', $route, $params);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $response = $client->getResponse()->getContent();
        $this->assertEquals([$recipient->name], json_decode($response));
    }
}
