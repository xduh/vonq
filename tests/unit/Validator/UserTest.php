<?php

namespace App\Tests\Unit\Validator\Util;

use App\Validator\User as UserValidator;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    /**
     * @dataProvider validInputs
     */
    public function testValid($name)
    {
        $validator = new UserValidator();
        $this->assertTrue($validator->validate($name));
    }

    public function validInputs()
    {
        return [
            ['ed'],
            ['sid'],
            ['atwentycharsnickname']
        ];
    }

    /**
     * @dataProvider invalidInputsWithMessages
     */
    public function testInvalid($name, $message)
    {
        $validator = new UserValidator();
        $this->assertFalse($validator->validate($name));

        $violations = ['name' => [$message]];
        $this->assertEquals($validator->getViolations(), $violations);
    }

    public function invalidInputsWithMessages()
    {
        return [
            [
                '',
                'This value should not be blank.'
            ],
            [
                'e',
                'This value is too short. It should have 2 characters or more.'
            ],
            [
                'atwenty1charsnickname',
                'This value is too long. It should have 20 characters or less.'
            ]
        ];
    }
}
