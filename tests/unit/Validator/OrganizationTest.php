<?php

namespace App\Tests\Unit\Validator\Util;

use App\Validator\Organization as OrganizationValidator;
use PHPUnit\Framework\TestCase;

class OrganizationTest extends TestCase
{
    /**
     * @dataProvider validInputs
     */
    public function testValid($name)
    {
        $validator = new OrganizationValidator();
        $this->assertTrue($validator->validate($name));
    }

    public function validInputs()
    {
        return [
            ['12345567890'],
            ['some organization name'],
            ['a very big organization name to use as tittle']
        ];
    }

    /**
     * @dataProvider invalidInputsWithMessages
     */
    public function testInvalid($name, $message)
    {
        $validator = new OrganizationValidator();
        $this->assertFalse($validator->validate($name));

        $violations = ['name' => [$message]];
        $this->assertEquals($validator->getViolations(), $violations);
    }

    public function invalidInputsWithMessages()
    {
        return [
            [
                '',
                'This value should not be blank.'
            ],
            [
                '123456789',
                'This value is too short. It should have 10 characters or more.'
            ],
            [
                '12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901',
                'This value is too long. It should have 100 characters or less.'
            ]
        ];
    }
}
