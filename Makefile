start:
	@docker-compose up -d

stop:
	@docker-compose down

build:
	@docker build -t vonq docker

prepare:
	@docker-compose run --rm composer install
	@docker-compose run -w "/app" app bin/console doctrine:database:create
	@docker-compose run -w "/app" app bin/console doctrine:migrations:migrate

test:
	@docker-compose run -w "/app" app bin/phpunit

.PHONY: start stop build prepare test