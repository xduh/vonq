<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Organization as OrganizationEntity;
use App\Entity\Subscription;
use App\Entity\User;
use App\Validator\Organization as OrganizationValidator;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

class Organization extends AbstractController
{
    public function create(Request $request)
    {
        if ($request->get('access_key') == '') {
            return new Response(
                json_encode(['errors' => 'Unauthorized']),
                401
            );
        }

        $name = trim($request->get('name'));

        $validator = new OrganizationValidator();

        if (!$validator->validate($name)) {
            $violations = $validator->getViolations();
            return new Response(json_encode(['errors' => $violations]), 400);
        }

        $organization = new OrganizationEntity();
        $organization->setName($request->get('name'));

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($organization);
        $entityManager->flush();

        $content = [
            'id' => $organization->getId(),
            'name' => $organization->getName()
        ];

        return new Response(json_encode($content), 201);
    }

    public function list(Request $request)
    {
        $organizations = $this->getDoctrine()
            ->getRepository(OrganizationEntity::class)
            ->findAll();

        $content = array_map(function ($group) {
            return $group->getName();
        }, $organizations);

        return new Response(json_encode($content), 200);
    }

    public function subscribe(Request $request)
    {
        if ($request->get('access_key') == '') {
            return new Response(
                json_encode(['errors' => 'Unauthorized']),
                401
            );
        }

        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->findByAccessKey($request->get('access_key'));

        if (count($user) < 1) {
            return new Response(
                json_encode(['errors' => 'Unauthorized']),
                401
            );
        }

        $organization = $this->getDoctrine()
            ->getRepository(OrganizationEntity::class)
            ->findOneBy(['id' => $request->get('organization_id')]);

        if (!$organization instanceof OrganizationEntity) {
            return new Response(
                json_encode(['errors' => 'Group not found']),
                404
            );
        }

        $subscription = new Subscription();
        $subscription->setOrganizationId($organization->getId());
        $subscription->setUserId($user[0]->getId());

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($subscription);

        try {
            $entityManager->flush();
        } catch (UniqueConstraintViolationException $e) {
            return new Response(
                json_encode(['errors' => 'Already subscribed to this group']),
                403
            );
        }

        return new Response(
            json_encode(['message' => 'Subscription created']),
            201
        );
    }

    public function leave(Request $request)
    {
        if ($request->get('access_key') == '') {
            return new Response(
                json_encode(['errors' => 'Unauthorized']),
                401
            );
        }

        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->findByAccessKey($request->get('access_key'));

        if (count($user) < 1) {
            return new Response(
                json_encode(['errors' => 'Unauthorized']),
                401
            );
        }

        $organization = $this->getDoctrine()
            ->getRepository(OrganizationEntity::class)
            ->findOneBy(['id' => $request->get('organization_id')]);

        if (!$organization instanceof OrganizationEntity) {
            return new Response(
                json_encode(['errors' => 'Group not found']),
                404
            );
        }

        $subscription = $this->getDoctrine()
            ->getRepository(Subscription::class)
            ->findOneBy([
                'organizationId' => $organization->getId(),
                'userId' => $user[0]->getId()
            ]);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($subscription);

        try {
            $entityManager->flush();
        } catch (Exception $e) {
            return new Response(
                json_encode(['errors' => $e->getMessage()]),
                403
            );
        }

        return new Response('', 204);
    }
}
