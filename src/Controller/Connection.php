<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Connection as ConnectionEntity;
use App\Entity\User;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

class Connection extends AbstractController
{
    public function list(Request $request)
    {
        if ($request->get('access_key') == '') {
            return new Response(
                json_encode(['errors' => 'Unauthorized']),
                401
            );
        }

        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->findByAccessKey($request->get('access_key'));

        $connections = $this->getDoctrine()
            ->getRepository(ConnectionEntity::class)
            ->findUserConnections($user[0]);

        $content = array_map(function ($user) {
            return $user->getName();
        }, $connections);

        return new Response(json_encode($content), 200);
    }

    public function userConnections(Request $request)
    {
        if ($request->get('access_key') == '') {
            return new Response(
                json_encode(['errors' => 'Unauthorized']),
                401
            );
        }

        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->findByName($request->get('name'));

        $connections = $this->getDoctrine()
            ->getRepository(ConnectionEntity::class)
            ->findUserConnections($user[0]);

        $content = array_map(function ($user) {
            return $user->getName();
        }, $connections);

        return new Response(json_encode($content), 200);
    }
}
