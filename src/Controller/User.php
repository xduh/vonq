<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Validator\User as UserValidator;
use App\Entity\User as UserEntity;

class User extends AbstractController
{
    public function create(Request $request)
    {
        $name = trim($request->get('name'));

        $validator = new UserValidator();

        if (!$validator->validate($name)) {
            $violations = $validator->getViolations();
            return new Response(json_encode(['errors' => $violations]), 400);
        }

        $user = new UserEntity();
        $user->setName($request->get('name'));

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();

        $content = [
            'id' => $user->getId(),
            'name' => $user->getName(),
            'access_key' => $user->getAccessKey()
        ];

        return new Response(json_encode($content), 201);
    }

    public function update(Request $request)
    {
        $name = trim($request->get('name'));

        $user = $this->getDoctrine()
            ->getRepository(UserEntity::class)
            ->findByAccessKey($request->get('access_key'));

        if (count($user) != 1) {
            return new Response(
                json_encode(['errors' => ['Unauthorized']]),
                401
            );
        }

        $validator = new UserValidator();

        if (!$validator->validate($name)) {
            $violations = $validator->getViolations();
            return new Response(json_encode(['errors' => $violations]), 400);
        }

        $user[0]->setName($request->get('name'));

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user[0]);
        $entityManager->flush();

        $content = [
            'id' => $user[0]->getId(),
            'name' => $user[0]->getName()
        ];

        return new Response(json_encode($content), 200);
    }

    public function list(Request $request)
    {
        $users = $this->getDoctrine()
            ->getRepository(UserEntity::class)
            ->findAll();

        $content = array_map(function ($user) {
            return $user->getName();
        }, $users);

        return new Response(json_encode($content), 200);
    }
}
