<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use App\Entity\Connection;
use App\Entity\Invite as InviteEntity;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

class Invite extends AbstractController
{
    public function create(Request $request)
    {
        $recipientName = trim($request->get('name'));

        if ($request->get('access_key') == '') {
            return new Response(
                json_encode(['errors' => 'Unauthorized']),
                401
            );
        }

        if ($recipientName == '') {
            return new Response(
                json_encode(['errors' => 'An username must be provided']),
                400
            );
        }

        $sender = $this->getDoctrine()
            ->getRepository(User::class)
            ->findByAccessKey($request->get('access_key'));

        if ($sender[0]->getName() == $recipientName) {
            return new Response(
                json_encode(['errors' => 'Can\'t connect with yourself']),
                403
            );
        }

        $recipient = $this->getDoctrine()
            ->getRepository(User::class)
            ->findByName($recipientName);

        $usersConnected = $this->getDoctrine()
            ->getRepository(Connection::class)
            ->checkUsersConnection($sender[0], $recipient[0]);

        if ($usersConnected) {
            return new Response(
                json_encode(['errors' => 'User already connected']),
                403
            );
        }

        if (count($recipient) == 0) {
            return new Response(
                json_encode(['errors' => 'Recipient not found']),
                404
            );
        }

        $invite = new InviteEntity();
        $invite->setSender($sender[0]->getId());
        $invite->setRecipient($recipient[0]->getId());

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($invite);
        try {
            $entityManager->flush();
        } catch (UniqueConstraintViolationException $e) {
            return new Response(
                json_encode(['errors' => 'User already invited']),
                403
            );
        }

        $content = [
            'recipient' => $recipient[0]->getName()
        ];

        return new Response(json_encode($content), 201);
    }

    public function list(Request $request)
    {
        if ($request->get('access_key') == '') {
            return new Response(
                json_encode(['errors' => 'Unauthorized']),
                401
            );
        }

        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->findByAccessKey($request->get('access_key'));

        $usersInvinted = $this->getDoctrine()
            ->getRepository(InviteEntity::class)
            ->findUsersInvited($user[0]);

        $content = array_map(function ($user) {
            return $user->getName();
        }, $usersInvinted);

        return new Response(json_encode($content), 200);
    }

    public function confirm(Request $request)
    {
        if ($request->get('access_key') == '') {
            return new Response(
                json_encode(['errors' => 'Unauthorized']),
                401
            );
        }

        $recipient = $this->getDoctrine()
            ->getRepository(User::class)
            ->findByAccessKey($request->get('access_key'));

        if (count($recipient) == 0) {
            return new Response(
                json_encode(['errors' => 'Unauthorized']),
                401
            );
        }

        $sender = $this->getDoctrine()
            ->getRepository(User::class)
            ->findByName($request->get('name'));

        if (count($sender) == 0) {
            return new Response(
                json_encode(['errors' => 'User not found']),
                404
            );
        }

        $invites = $this->getDoctrine()
            ->getRepository(InviteEntity::class)
            ->findInviteRequests($sender[0], $recipient[0]);

        if ($invites == 0) {
            return new Response(
                json_encode(['errors' => 'Invite not found']),
                404
            );
        }

        $invites = $this->getDoctrine()
            ->getRepository(Connection::class)
            ->createConnection($invites[0]);

        $content = ['message' => 'Connection created'];
        return new Response(json_encode($content), 200);
    }
}
