<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Meeting as MeetingEntity;
use App\Validator\Meeting as MeetingValidator;
use App\Entity\User;
use App\Entity\Organization;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

class Meeting extends AbstractController
{
    public function create(Request $request)
    {
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneBy(['accessKey' => $request->get('access_key')]);

        if ($request->get('access_key') == '' || !$user instanceof User) {
            return new Response(
                json_encode(['errors' => 'Unauthorized']),
                401
            );
        }

        $title = trim($request->get('title'));

        $validator = new MeetingValidator();

        $date = new \DateTime($request->get('date'));

        if (!$validator->validate($title, $date)) {
            $violations = $validator->getViolations();
            return new Response(json_encode(['errors' => $violations]), 400);
        }

        $organization = $this->getDoctrine()
            ->getRepository(Organization::class)
            ->findOneBy(['id' => $request->get('organization_id')]);

        if (!$organization instanceof Organization) {
            return new Response(
                json_encode(['errors' => 'Group not found']),
                404
            );
        }

        $meeting = new MeetingEntity();
        $meeting->setOrganizationId($organization->getId());
        $meeting->setTitle($user->getId());
        $meeting->setDate($date);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($meeting);

        $entityManager->flush();

        $content = [
            'id' => $meeting->getId(),
            'title' => $meeting->getTitle(),
            'date' => $meeting->getDate()->format('Y-m-d H:i:s')
        ];

        return new Response(json_encode($content), 201);
    }
}
