<?php

namespace App\Repository;

use App\Entity\Connection as ConnectionEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Entity\Invite;
use App\Entity\User;
use Doctrine\ORM\Query\ResultSetMapping;

/**
 * @method Connection|null find($id, $lockMode = null, $lockVersion = null)
 * @method Connection|null findOneBy(array $criteria, array $orderBy = null)
 * @method Connection[]    findAll()
 * @method Connection[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class Connection extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ConnectionEntity::class);
    }

    // @return boolean
    public function createConnection(Invite $invite)
    {
        $userA = $invite->getSender();
        $userB = $invite->getRecipient();
        if ($invite->getSender() > $invite->getRecipient()) {
            $userA = $invite->getRecipient();
            $userB = $invite->getSender();
        }

        $em = $this->getEntityManager();
        $em->getConnection()->beginTransaction();
        try {
            $connection = new ConnectionEntity;
            $connection->setUserA($userA);
            $connection->setUserB($userB);
            $em->persist($connection);

            $delete = $em->createQueryBuilder();
            $delete->delete('App\Entity\Invite', 'i');
            $delete->orWhere('i.sender = :userA AND i.recipient = :userB');
            $delete->orWhere('i.sender = :userB AND i.recipient = :userA');

            $delete->setParameter('userA', $userA);
            $delete->setParameter('userB', $userB);

            $query = $delete->getQuery();
            $result = $query->getResult();


            $em->flush();
            $em->getConnection()->commit();
        } catch (Exception $e) {
            $em->getConnection()->rollBack();
            return false;
        }

        return true;
    }

    // @return boolean
    public function checkUsersConnection(User $sender, User $recipient)
    {
        $userA = $sender->getId();
        $userB = $recipient->getId();
        if ($sender->getId() > $recipient->getId()) {
            $userA = $sender->getId();
            $userB = $recipient->getId();
        }

        $connections = $this->findBy(['userA' => $userA, 'userB' => $userB]);

        if (count($connections) > 0) {
            return true;
        }

        return false;
    }

    // @return UserEntity[] Returns an array of User objects
    public function findUserConnections(User $user)
    {
        $rsm = new ResultSetMapping;
        $rsm->addEntityResult('App\Entity\User', 'u');
        $rsm->addFieldResult('u', 'id', 'id');
        $rsm->addFieldResult('u', 'name', 'name');
        $rsm->addFieldResult('u', 'access_key', 'accessKey');
        $rsm->addFieldResult('u', 'created_at', 'createdAt');
        $rsm->addFieldResult('u', 'updated_at', 'updatedAt');

        $query = $this->getEntityManager()->createNativeQuery("SELECT id, name, access_key, created_at, updated_at FROM user WHERE id IN (SELECT DISTINCT(if(user_a = :id, user_b, user_a)) as connected FROM connection WHERE user_a = :id OR user_b = :id)", $rsm);
        $query->setParameter('id', $user->getId());

        return $query->getResult();
    }
}
