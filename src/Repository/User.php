<?php

namespace App\Repository;

use App\Entity\User as UserEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class User extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserEntity::class);
    }

    // /**
    //  * @return UserEntity[] Returns an array of User objects
    //  */
    public function findByAccessKey($accessKey)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.accessKey = :accessKey')
            ->setParameter('accessKey', $accessKey)
            ->setMaxResults(1)
            ->getQuery()
            ->getResult()
        ;
    }

    // /**
    //  * @return UserEntity[] Returns an array of User objects
    //  */
    public function findByName($name)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.name = :name')
            ->setParameter('name', $name)
            ->setMaxResults(1)
            ->getQuery()
            ->getResult()
        ;
    }
}
