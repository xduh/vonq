<?php

namespace App\Repository;

use App\Entity\Invite as InviteEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Entity\User;
use Doctrine\ORM\Query\ResultSetMapping;

/**
 * @method Invite|null find($id, $lockMode = null, $lockVersion = null)
 * @method Invite|null findOneBy(array $criteria, array $orderBy = null)
 * @method Invite[]    findAll()
 * @method Invite[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class Invite extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, InviteEntity::class);
    }

    //  * @return User[] Returns an array of User objects
    //  */
    public function findUsersInvited(User $user)
    {
        $rsm = new ResultSetMapping;
        $rsm->addEntityResult('App\Entity\User', 'u');
        $rsm->addFieldResult('u', 'id', 'id');
        $rsm->addFieldResult('u', 'name', 'name');
        $rsm->addFieldResult('u', 'access_key', 'accessKey');
        $rsm->addFieldResult('u', 'created_at', 'createdAt');
        $rsm->addFieldResult('u', 'updated_at', 'updatedAt');

        $query = $this->getEntityManager()->createNativeQuery("SELECT id, name, access_key, created_at, updated_at FROM user WHERE id IN (SELECT DISTINCT(if(recipient = :id, sender, recipient)) as invited FROM invite WHERE sender = :id OR recipient = :id)", $rsm);
        $query->setParameter('id', $user->getId());

        return $query->getResult();
    }

    //  * @return InviteEntity[] Returns an array of Invite objects
    //  */
    public function findInviteRequests(User $sender, User $recipient)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.sender = :sender')
            ->andWhere('u.recipient = :recipient')
            ->setParameter('sender', $sender->getId())
            ->setParameter('recipient', $recipient->getId())
            ->setMaxResults(1)
            ->getQuery()
            ->getResult()
        ;
    }
}
