<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Connection")
 * @ORM\HasLifecycleCallbacks
 */
class Connection
{
   /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $userA;

    /**
     * @ORM\Column(type="integer")
     */
    private $userB;

    /**
     * @ORM\Column(type="datetimetz" )
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetimetz")
     */
    private $updatedAt;

    public function getId()
    {
        return $this->id;
    }

    public function getUserA()
    {
        return $this->userA;
    }

    public function setUserA($userA)
    {
        $this->userA = $userA;
    }

    public function getUserB()
    {
        return $this->userB;
    }

    public function setUserB($userB)
    {
        $this->userB = $userB;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
    */
    public function updatedTimestamps(): void
    {
        $this->setUpdatedAt(new \DateTime('now'));
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }
}
