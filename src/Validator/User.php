<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Validation;

class User
{
    private $violations;

    public function validate($name)
    {
        $validator = Validation::createValidator();
        $nameViolations = $validator->validate($name, [
            new Length(['min' => 2]),
            new Length(['max' => 20]),
            new NotBlank(),
        ]);

        if (0 !== count($nameViolations)) {
            $this->violations['name'] = [];
            foreach ($nameViolations as $error) {
                $this->violations['name'][] = $error->getMessage();
            }
            return false;
        }

        return true;
    }

    public function getViolations()
    {
        return $this->violations;
    }
}
