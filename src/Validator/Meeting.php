<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Validation;

class Meeting
{
    private $violations;

    public function validate($title, $date)
    {
        $validator = Validation::createValidator();
        $titleViolations = $validator->validate($title, [
            new Length(['min' => 10]),
            new Length(['max' => 100]),
            new NotBlank(),
        ]);

        $dateViolations = $validator->validate($date, [
            new GreaterThanOrEqual(new \DateTime('now')),
            new NotBlank()
        ]);

        if (0 !== count($titleViolations) || 0 !== count($dateViolations)) {
            $this->violations['title'] = [];
            foreach ($titleViolations as $error) {
                $this->violations['title'][] = $error->getMessage();
            }
            $this->violations['date'] = [];
            foreach ($dateViolations as $error) {
                $this->violations['date'][] = $error->getMessage();
            }
            return false;
        }

        return true;
    }

    public function getViolations()
    {
        return $this->violations;
    }
}
